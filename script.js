function switchForm() {
    var loginForm = document.getElementById('loginForm');
    var registerForm = document.getElementById('registerForm');
    var switchFormText = document.getElementById('switchFormText');

    if (loginForm.style.display === 'none') {
        loginForm.style.display = 'block';
        registerForm.style.display = 'none';
        switchFormText.innerHTML = '¿Quieres crear una cuenta nueva? <a href="#" onclick="switchForm()">Regístrate</a>';
    } else {
        loginForm.style.display = 'none';
        registerForm.style.display = 'block';
        switchFormText.innerHTML = '¿Ya estás registrado? <a href="#" onclick="switchForm()">Inicia Sesión</a>';
    }
}

function login() {
    var email = document.getElementById('loginEmail').value;
    var password = document.getElementById('loginPassword').value;

    // Aquí puedes verificar los datos y realizar acciones de autenticación

    alert('Inicio de sesión exitoso para "' + email +'"');
}

function register() {
    var name = document.getElementById('registerName').value;
    var email = document.getElementById('registerEmail').value;
    var password = document.getElementById('registerPassword').value;

    // Aquí puedes guardar los datos de registro (en una base de datos o localStorage)

    alert('El usuario "' + name +'" se ha registrado correctamente');
}
